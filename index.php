<?php
    require('animal.php');
    require('Ape.php');
    require('Frog.php');

    $sheep = new Animal("shaun");
    echo "Name : ".$sheep->name."<br>"; // "shaun"
    echo "legs : ".$sheep->legs."<br>"; // 4
    echo "cold blooded : ".$sheep->cold_blooded."<br><br>"; // "no"

    $kodok = new Frog("buduk");
    echo "Name : ".$kodok->name."<br>"; // "shaun"
    echo "legs : ".$kodok->legs."<br>"; // 4
    echo "cold blooded : ".$kodok->cold_blooded."<br>";
    echo "Jump : ".$kodok->jump()."<br><br>";
    $kodok->jump() ; // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "Name : ".$sungokong->name."<br>"; // "shaun"
    echo "legs : ".$sungokong->legs."<br>"; // 4
    echo "cold blooded : ".$sungokong->cold_blooded."<br>";
    echo "Yell : ".$sungokong->yell()."<br><br>"; // "Auooo"

    
?>